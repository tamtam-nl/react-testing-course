const chunk = require('./index');

test('function chunk exists', () => {

});

test('chunk divides an array of 12 elements with chunk size 4', () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const chunked = chunk(arr, 4);


});

test('chunk divides an array of 16 elements with chunk size 6', () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
  const chunked = chunk(arr, 6);


});

test('chunk divides an array of 20 elements contains [7, 8, 9, 10, 11, 12] ', () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  const chunked = chunk(arr, 6);

});

test('chunk divides an empty array with chunk size 5', () => {
  const arr = [];
  const chunked = chunk(arr, 5);

});


test('chunk divides an array of 10 elements with chunk size 1', () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const chunked = chunk(arr, 1);
});
